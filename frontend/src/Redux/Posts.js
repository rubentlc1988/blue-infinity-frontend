

import * as actionTypes from './ActionTypes';

const initialState = {
    posts: []
};

//reducer
export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_POSTS:
            return { 
                ...state, 
                posts: action.payload
            };
        case actionTypes.ALL_UPDATE_POST:
            return {
                ...state,         
                posts: state.posts.map(post =>{
                    if(post.id === action.payload.id) {
                        post = action.payload
                    }
                    return post;
                })       
            }
        case actionTypes.CREATE_POST:
            return {
                ...state,
                posts: state.posts.concat(action.payload)   
            };
        default:
            return state;
    }
};

//actions
export const fetchPosts = (posts) => ({
    type: actionTypes.FETCH_POSTS,
    payload: posts
});

export const updatePost = (post) => ({
    type: actionTypes.ALL_UPDATE_POST,
    payload: post
});

export const createPost = (post) => ({
    type: actionTypes.CREATE_POST,
    payload: post
});