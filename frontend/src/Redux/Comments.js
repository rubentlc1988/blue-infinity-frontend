

import * as actionTypes from './ActionTypes';

const initialState = {
    comments: []
};

//reducer
export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_COMMENTS:
            return { 
                ...state, 
                comments: action.payload
            };
        case actionTypes.ALL_UPDATE_COMMENT:
            return {
                ...state,
                comments: state.comments.map(comment =>{
                    if(comment.id === action.payload.id) {
                        comment = action.payload;
                    }
                    return comment;
                })
            };
        case actionTypes.CREATE_COMMENT:
            return {
                ...state,
                comments: state.comments.concat(action.payload)   
            };
        default:
            return state;
    }
};

//actions
export const fetchComments = (comments) => ({
    type: actionTypes.FETCH_COMMENTS,
    payload: comments
});

export const updateComment = (comments) => ({
    type: actionTypes.ALL_UPDATE_COMMENT,
    payload: comments
});

export const createComment = (comment) => ({
    type: actionTypes.CREATE_COMMENT,
    payload: comment
});