

import * as actionTypes from './ActionTypes';

const initialState = {
    comment: {},
    showDialogEditComment: false,
    showDialogCreateComment: false
};

//reducer
export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_COMMENT:
            return { 
                ...state, 
                comment: action.payload
            };
        case actionTypes.SHOW_DIALOG_EDIT_COMMENT:
            return {
                ...state,
                showDialogEditComment: action.payload
            };
        case actionTypes.SHOW_DIALOG_CREATE_COMMENT:
            return {
                ...state,
                showDialogCreateComment: action.payload
            };
        default:
            return state;
    }
};

//actions
export const fetchComment = (comments) => ({
    type: actionTypes.FETCH_COMMENT,
    payload: comments
});

export const showDialogEditComment = (show) => ({
    type: actionTypes.SHOW_DIALOG_EDIT_COMMENT,
    payload: show
});

export const showDialogCreateComment = (show) => ({
    type: actionTypes.SHOW_DIALOG_CREATE_COMMENT,
    payload: show
});