
import * as actionTypes from './ActionTypes';

const initialState = {
    categories: []
};

//reducer
export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CATEGORIES:
            return { 
                ...state, 
                categories: action.payload
            };
        default:
            return state;
    }
};

//actions
export const fetchCategories = (categories) => ({
    type: actionTypes.FETCH_CATEGORIES,
    payload: categories
});