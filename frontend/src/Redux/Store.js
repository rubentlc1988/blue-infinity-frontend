import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from 'redux-thunk';

import categoriesReducer from "./Categories";
import postsReducer from "./Posts";
import postReducer from "./Post";
import commentsReducer from "./Comments";
import commentReducer from "./Comment";
import pageTitleReducer from "./PageTitle";

const middleware = applyMiddleware(thunkMiddleware);

const combine = combineReducers({
    categoriesReducer,
    postsReducer,
    postReducer,
    pageTitleReducer,
    commentsReducer,
    commentReducer
});

export default createStore(combine, middleware);