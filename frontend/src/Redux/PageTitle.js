

import * as actionTypes from './ActionTypes';

const initialState = {
    title: '',
    errorPage: false,
};

//reducer
export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_TITLE:
            return { 
                ...state, 
                title: action.payload
            };
        case actionTypes.ERROR_PAGE:
            return { 
                ...state, 
                errorPage: action.payload
            };
        default:
            return state;
    }
};

//actions
export const changePageTitle = (title) => ({
    type: actionTypes.CHANGE_TITLE,
    payload: title
});

export const isErrorPage = (error) => ({
    type: actionTypes.ERROR_PAGE,
    payload: error
});