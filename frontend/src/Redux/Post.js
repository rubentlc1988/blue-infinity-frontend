

import * as actionTypes from './ActionTypes';

const initialState = {
    post: {},
    showDialogEditPost: false,
    showDialogCreatePost: false
};

//reducer
export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_POST:
            return { 
                ...state, 
                post: action.payload
            };
        case actionTypes.UPDATE_POST_COMMENTS:
            return {
                ...state,
                post: {...state.post, commentCount: action.payload.count}
            };
        case actionTypes.SHOW_DIALOG_EDIT_POST:
            return {
                ...state,
                showDialogEditPost: action.payload
            };
        case actionTypes.SHOW_DIALOG_CREATE_POST:
            return {
                ...state,
                showDialogCreatePost: action.payload
            };
        default:
            return state;
    }
};

//actions

export const fetchPost = (post) => ({
    type: actionTypes.FETCH_POST,
    payload: post
});

export const updatePostComments = (postId, count) => ({
    type: actionTypes.UPDATE_POST_COMMENTS,
    payload: {postId, count}
});

export const showDialogEditPost = (show) => ({
    type: actionTypes.SHOW_DIALOG_EDIT_POST,
    payload: show
});

export const showDialogCreatePost = (show) => ({
    type: actionTypes.SHOW_DIALOG_CREATE_POST,
    payload: show
});