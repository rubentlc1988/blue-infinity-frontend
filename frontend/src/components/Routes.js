import React from 'react'
import { Route, Switch } from 'react-router-dom'
import MainPage from './Pages/MainPage';
import Page404 from './Pages/Page404';
import PostDetailPage from './Pages/PostDetailPage';

const Routes = () => (
    <main>
        <Switch>
            <Route exact path='/' component={MainPage} />
            <Route exact path='/Page404' component={Page404} />
            <Route exact path='/:category' component={MainPage} />
            <Route exact path='/:category/:id' component={PostDetailPage} />
            <Route component={Page404} />
        </Switch>
    </main>
)

export default Routes
