import React, { Component } from 'react';
import { connect } from 'react-redux';

import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';

import { Link } from 'react-router-dom'

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }

    handleDrawerOpen = () => {
        this.setState({ open: !this.state.open });
    };

    handleClose = () => {
        this.setState({ open: false })
    };

    handlerDrawerChange = (open) => {
        this.setState({ open })
    }

    render() {
        const categories = this.props.categoriesReducer.categories || [];
        const pageTitle = this.props.pageTitleReducer.title || '';
        const errorPage = this.props.pageTitleReducer.errorPage;

        return (
            <div>
                <Drawer
                    docked={false}
                    open={this.state.open}
                    onRequestChange={this.handlerDrawerChange} >

                    <MenuItem
                        containerElement={<Link to='/' />}
                        primaryText='All Posts'
                        key='All Posts'
                        onClick={this.handleClose}
                    />

                    {categories.map((categorie) => (
                        <MenuItem
                            containerElement={<Link to={`/${categorie.name}`} />}
                            primaryText={categorie.name}
                            key={categorie.name}
                            onClick={this.handleClose} />
                    ))}
                </Drawer>

                { errorPage ? 
                    null : 
                    <AppBar
                        title={pageTitle}
                        onLeftIconButtonClick={this.handleDrawerOpen}
                    />
                }
                
            </div>
        );
    }
}

const mapStateToProps = ({ categoriesReducer, pageTitleReducer }) => {
    return {
        categoriesReducer,
        pageTitleReducer
    };
}

export default connect(mapStateToProps)(Header);