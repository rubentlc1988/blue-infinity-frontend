import React, { Component } from 'react';
import Header from './Header';
import Routes from './Routes'
import { withRouter } from 'react-router-dom'
import { connect } from "react-redux";
import * as categoriesActions from './../Redux/Categories';

import * as API from './../Api'

class App extends Component {

    componentDidMount() {
        API.getCategories().then( res => {
            this.props.FetchCategories(res);
        });
    };

    render() {
        return (
            <div>
                <Header />
                <Routes />
            </div>
        );
    }
}

const mapStateToProps = () => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        FetchCategories: (categories) => dispatch(categoriesActions.fetchCategories(categories))
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
