import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as commentsActions from './../Redux/Comments';
import * as commentActions from './../Redux/Comment';
import * as postActions from './../Redux/Post';

import EditCommentDialog from './Dialogs/EditCommentDialog';

import { Card, CardActions, CardHeader } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';


import * as API from './../Api';

class CommentsList extends Component {

    state = {
        commentToEdit : null,
        showEditCommentDialog: false
    }

    componentDidMount() {
        API.getPostComments(this.props.postId)
            .then(res => this.props.FetchComments(res))
    };

    componentDidUpdate() {
        //update contagem de comentarios de um post consuante se apaga ou acrescenta comentarios
        let comments = this.props.commentsReducer.comments;
        comments = comments.filter(post => !post.deleted);
        this.props.UpdatePostComments(this.props.postId, comments.length);
    };

    handleCommentScore = (commentId, option) => {
        API.commentVote(commentId, option)
            .then(comment => this.props.UpdateComment(comment));
    };

    handleCommentDelete = (commentId) => {
        API.commentDelete(commentId)
            .then(comment => this.props.UpdateComment(comment));
    };

    handleCommentEdit = (comment) => {
        this.props.EditComment(comment)
    };

    render() {
        let comments = this.props.commentsReducer.comments || [];
        comments = comments.filter(post => !post.deleted);
        
        if (comments.length === 0) {
            return <h1>No comments to show!</h1>
        }
        
        return (
            <div>
                <h1>Comments:</h1>
                {
                    comments.map((comment) => (
                        <Card key={comment.id}>
                            <CardHeader
                                title={comment.body}
                                subtitle={`by: ${comment.author}`}
                            />
                            <CardHeader
                                title={`Score: ${comment.voteScore}`}
                            />
                            <CardActions>
                                <FlatButton label="SCORE UP" onClick={() => this.handleCommentScore(comment.id, 'upVote')} />
                                <FlatButton label="SCORE DOWN" onClick={() => this.handleCommentScore(comment.id, 'downVote')} />
                                <FlatButton label="EDIT" onClick={() => this.handleCommentEdit(comment)} />
                                <FlatButton label="DELETE" onClick={() => this.handleCommentDelete(comment.id)} />
                            </CardActions>
                        </Card>
                    ))
                }

                <EditCommentDialog />
                
            </div>
        );
    };
};

const mapStateToProps = ({commentsReducer}) => {
    return {
        commentsReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        FetchComments: (comments) => dispatch(commentsActions.fetchComments(comments)),
        UpdateComment: (comment) => dispatch(commentsActions.updateComment(comment)),
        UpdatePostComments: (postId,count) => dispatch(postActions.updatePostComments(postId,count)),
        EditComment: (comment) => {
            dispatch(commentActions.fetchComment({...comment}));
            dispatch(commentActions.showDialogEditComment(true));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentsList);