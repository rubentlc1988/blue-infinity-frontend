import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postActions from './../Redux/Post';

import { Link } from 'react-router-dom'
import { Card, CardActions, CardHeader, CardTitle } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

class Post extends Component {

    handleEdit = (post) => {
        this.props.EditPost(post);
    }

    render() {
        const { post, isPostList, handlePostScore, handlePostDelete } = this.props;
        
        let detailButton = null;
        let firstTitle = null;
        let secondTitle = null;
        if (isPostList) {
            detailButton = <FlatButton label="SEE POST DETAIL" containerElement={<Link to={`/${post.category}/${post.id}`} />}/>;
            firstTitle =  <CardTitle title={post.title} subtitle={`by: ${post.author}`} />
        } else {
            firstTitle = <CardTitle title={post.title} />
            secondTitle = <CardTitle title={post.body} subtitle={`by: ${post.author}`} />
        }

        return (
            <div>
                <Card>
                    {firstTitle}
                    {secondTitle}
                    <CardHeader
                        title={`Score: ${post.voteScore}`}
                        subtitle={`(${post.commentCount} comments)`}
                    />
                    <CardActions>
                        {detailButton}
                        <FlatButton label="SCORE UP" onClick={() => handlePostScore(post.id, 'upVote')} />
                        <FlatButton label="SCORE DOWN" onClick={() => handlePostScore(post.id, 'downVote')} />
                        <FlatButton label="EDIT" onClick={() => this.handleEdit(post)} />
                        <FlatButton label="DELETE" onClick={() => handlePostDelete(post.id)} />
                    </CardActions>
                </Card>
            </div>
        );
    };
};

const mapStateToProps = () => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        EditPost: (post) => {
            dispatch(postActions.fetchPost({...post}));
            dispatch(postActions.showDialogEditPost(true));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Post);