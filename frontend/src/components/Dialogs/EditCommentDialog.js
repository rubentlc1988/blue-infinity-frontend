import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as commentsActions from './../../Redux/Comments';
import * as commentActions from './../../Redux/Comment';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

import * as API from './../../Api';

class EditCommentDialog extends Component {

    state = {
        body: '',
        bodyError: ''
    }

    handleSave = () => {
        if (this.state.bodyError.trim().length === 0) {
            const {comment} = this.props.commentReducer;
            const body = this.state.body || comment.body;

            const newComment = {
                timestamp: new Date().getTime(),
                body: body,
            }

            API.commentUpdate(comment.id, newComment)
                .then(comment => this.props.UpdateComment(comment))
                .then(() => this.closeDialog())
                .catch(e => console.error('Error: ', e));
        }
    };

    closeDialog = () => {
        this.setState({
            body: '',
            bodyError: ''
        });
        this.props.CloseDialog();
    };

    handleClose = () => {
        this.closeDialog();
    };

    handleBodyTextChange = (event, newValue) => {
        this.setState({
            body: newValue,
            bodyError: newValue.trim().length === 0 ? 'Fill body of post!' : ''
        });
    }

    render() {
        const editActions = [
            <FlatButton label="Save" primary={true} onClick={this.handleSave} />,
            <FlatButton label="Cancel" primary={true} onClick={this.handleClose} />,
        ];

        const {comment, showDialogEditComment} = this.props.commentReducer;

        return (
            <div>
                <Dialog
                    actions={editActions}
                    modal={false}
                    open={showDialogEditComment}
                    onRequestClose={this.handleClose}>
                    <div>
                        <h1>EDIT COMMENT</h1>
                        <TextField
                            floatingLabelText="Body:"
                            defaultValue={comment.body}
                            fullWidth={true}
                            errorText={this.state.bodyError}
                            onChange={this.handleBodyTextChange} />
                    </div>
                </Dialog>
            </div>
        );
    }
};

const mapStateToProps = ({commentReducer}) => {
    return {
        commentReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        UpdateComment: (comment) => dispatch(commentsActions.updateComment(comment)),
        CloseDialog: () => dispatch(commentActions.showDialogEditComment(false))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditCommentDialog);