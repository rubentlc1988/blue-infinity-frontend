import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postsActions from './../../Redux/Posts';
import * as postActions from './../../Redux/Post';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import * as API from './../../Api';
const uuidv1 = require('uuid/v1');

class AddPost extends Component {

    state = {
        title: '',
        body: '',
        author: '',
        category: '',
        titleError: '',
        bodyError: '',
        authorError: '',
        categoryError: '',
    }

    handleSave = () => {
        this.setState({
            titleError: this.state.title.trim().length === 0 ? 'Fill title of post!' : '',
            bodyError: this.state.body.trim().length === 0 ? 'Fill body of post!' : '',
            authorError: this.state.author.trim().length === 0 ? 'Fill author of post!' : '',
            categoryError: this.state.category.trim().length === 0 ? 'Select category of post!' : ''
        });

        if (this.state.title.trim().length === 0 || 
            this.state.body.trim().length === 0 ||
            this.state.author.trim().length === 0 ||
            this.state.category.trim().length === 0 ) { 
                return;
        }
        
        const post = {
            id: uuidv1(),
            timestamp: new Date().getTime(),
            title: this.state.title,
            body: this.state.body,
            author: this.state.author,
            category: this.state.category
        };

        API.postCreate(post)
            .then(post => this.props.CreatePost(post))
            .then(() => this.closeDialog())
            .catch(e => console.error('Error: ', e));
    };

    closeDialog = () => {
        this.setState({
            title: '',
            body: '',
            author: '',
            category: '',
            titleError: '',
            bodyError: '',
            authorError: '',
            categoryError: '',
        });
        this.props.CloseDialog();
    };

    handleClose = () => {
        this.closeDialog();
    };

    handleTitleTextChange = (event, newValue) => {
        this.setState({
            title: newValue,
            titleError: newValue.trim().length === 0 ? 'Fill title of post!' : ''
        });
    }

    handleBodyTextChange = (event, newValue) => {
        this.setState({
            body: newValue,
            bodyError: newValue.trim().length === 0 ? 'Fill body of post!' : ''
        });
    }

    handleAuthorTextChange = (event, newValue) => {
        this.setState({
            author: newValue,
            authorError: newValue.trim().length === 0 ? 'Fill author of post!' : ''
        });
    }

    handleCategoryChange = (event, index, value) => {
        this.setState({
            category: value,
            categoryError: ''
        });
    }

    render() {
        const editActions = [
            <FlatButton label="Save" primary={true} onClick={this.handleSave} />,
            <FlatButton label="Cancel" primary={true} onClick={this.handleClose} />,
        ];

        const categories = this.props.categoriesReducer.categories || [];
        const {showDialogCreatePost} = this.props.postReducer;

        return (
            <div>
                <Dialog
                    actions={editActions}
                    modal={false}
                    open={showDialogCreatePost}
                    onRequestClose={this.handleClose}>
                    <div>
                        <h1>CREATE POST</h1>

                        <TextField
                            floatingLabelText="Title:"
                            fullWidth={true}
                            errorText={this.state.titleError}
                            onChange={this.handleTitleTextChange} />
                        <br />
                        <TextField
                            floatingLabelText="Body:"
                            fullWidth={true}
                            errorText={this.state.bodyError}
                            onChange={this.handleBodyTextChange} />
                        <br />
                        <TextField
                            floatingLabelText="Author:"
                            fullWidth={true}
                            errorText={this.state.authorError}
                            onChange={this.handleAuthorTextChange} />
                        <br />
                        <SelectField
                            floatingLabelText="Category:"
                            fullWidth={true}
                            value={this.state.category}
                            errorText={this.state.categoryError}
                            onChange={this.handleCategoryChange} >
                                {categories.map((categorie) => (
                                    <MenuItem
                                        primaryText={categorie.name}
                                        value={categorie.name}
                                        key={categorie.name}/>
                                ))}
                        </SelectField>
                    </div>
                </Dialog>
            </div>
        );
    }
};

const mapStateToProps = ({ categoriesReducer, postReducer}) => {
    return {
        categoriesReducer ,
        postReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        CreatePost: (post) => dispatch(postsActions.createPost(post)),
        CloseDialog: () => dispatch(postActions.showDialogCreatePost(false))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);