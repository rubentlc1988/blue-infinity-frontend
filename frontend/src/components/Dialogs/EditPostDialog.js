import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postsActions from './../../Redux/Posts';
import * as postActions from './../../Redux/Post';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

import * as API from './../../Api';

class EditPost extends Component {
    
    state = {
        title: '',
        body: '',
        titleError: '',
        bodyError: '',
    }

    handleSave = () => {
        if (this.state.titleError.trim().length === 0 && this.state.bodyError.trim().length === 0) {
            const {post} = this.props.postReducer;
            const title = this.state.title || post.title;
            const body = this.state.body || post.body;
            const newPost = {title, body};

            API.postUpdate(post.id, newPost)
                .then(post => {
                    this.props.UpdatePost(post);
                    this.props.FetchPost(post)
                })
                .then(() => this.closeDialog())
                .catch(e => console.error('Error: ', e));
        }
    };

    closeDialog = () => {
        this.setState({
            title: '',
            body: '',
            titleError: '',
            bodyError: ''
        });
        this.props.CloseDialog();
    };

    handleClose = () => {
        this.closeDialog();
    };

    handleTitleTextChange = (event, newValue) => {
        this.setState({
            title: newValue,
            titleError: newValue.trim().length === 0 ? 'Fill title of post!' : ''
        });
    };

    handleBodyTextChange = (event, newValue) => {
        this.setState({
            body: newValue,
            bodyError: newValue.trim().length === 0 ? 'Fill body of post!' : ''
        })
    };

    render() {
        const editActions = [
            <FlatButton label="Save" primary={true} onClick={this.handleSave} />,
            <FlatButton label="Cancel" primary={true} onClick={this.handleClose} />,
        ];

        const {post, showDialogEditPost} = this.props.postReducer;
        
        return (
            <div>
                <Dialog
                    actions={editActions}
                    modal={false}
                    open={showDialogEditPost}
                    onRequestClose={this.handleClose}>
                    <div>
                        <h1>EDIT POST</h1>

                        <TextField
                            floatingLabelText="Title:"
                            defaultValue={post.title}
                            errorText={this.state.titleError}
                            fullWidth={true}
                            onChange={this.handleTitleTextChange}/>
                        <br />
                        <TextField
                            floatingLabelText="Body:"
                            defaultValue={post.body}
                            errorText={this.state.bodyError}
                            fullWidth={true} 
                            onChange={this.handleBodyTextChange}/>
                    </div>
                </Dialog>
            </div>
        );
    }
};

const mapStateToProps = ({postReducer}) => {
    return {  
        postReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        UpdatePost: (post) => dispatch(postsActions.updatePost(post)),
        FetchPost: (post) => dispatch(postActions.fetchPost(post)),
        CloseDialog: () => dispatch(postActions.showDialogEditPost(false))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPost);