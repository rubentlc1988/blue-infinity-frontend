import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as commentsActions from './../../Redux/Comments';
import * as commentActions from './../../Redux/Comment';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

import * as API from './../../Api';
const uuidv1 = require('uuid/v1');

class AddCommentDialog extends Component {

    state = {
        body: '',
        author: '',
        bodyError: '',
        authorError: '',
    }

    handleSave = () => {
        this.setState({
            bodyError: this.state.body.trim().length === 0 ? 'Fill body of comment!' : '',
            authorError: this.state.author.trim().length === 0 ? 'Fill author of comment!' : '',
        });

        if (this.state.body.trim().length === 0 || this.state.author.trim().length === 0) { 
            return;
        }
        
        const comment = {
            id: uuidv1(),
            timestamp: new Date().getTime(),
            body: this.state.body,
            author: this.state.author,
            parentId: this.props.postId
        };

        API.commentCreate(comment)
            .then(comment => this.props.CreateComment(comment))
            .then(() => this.closeDialog())
            .catch(e => console.error('Error: ', e));
    };

    closeDialog = () => {
        this.setState({
            body: '',
            author: '',
            bodyError: '',
            authorError: '',
        });
        this.props.CloseDialog();
    };

    handleClose = () => {
        this.closeDialog();
    };

    handleBodyTextChange = (event, newValue) => {
        this.setState({
            body: newValue,
            bodyError: newValue.trim().length === 0 ? 'Fill body of comment!' : ''
        });
    }

    handleAuthorTextChange = (event, newValue) => {
        this.setState({
            author: newValue,
            authorError: newValue.trim().length === 0 ? 'Fill author of comment!' : ''
        });
    }


    render() {
        const editActions = [
            <FlatButton label="Save" primary={true} onClick={this.handleSave} />,
            <FlatButton label="Cancel" primary={true} onClick={this.handleClose} />,
        ];

        const {showDialogCreateComment} = this.props.commentReducer;

        return (
            <div>
                <Dialog
                    actions={editActions}
                    modal={false}
                    open={showDialogCreateComment}
                    onRequestClose={this.handleClose}>
                    <div>
                        <h1>CREATE COMMENT</h1>

                        <TextField
                            floatingLabelText="Author:"
                            fullWidth={true}
                            errorText={this.state.authorError}
                            onChange={this.handleAuthorTextChange} />
                        <br />
                        <TextField
                            floatingLabelText="Body:"
                            fullWidth={true}
                            errorText={this.state.bodyError}
                            onChange={this.handleBodyTextChange} />
                        <br />
                    </div>
                </Dialog>
            </div>
        );
    }
};

const mapStateToProps = ({commentReducer}) => {
    return {
        commentReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        CreateComment: (post) => dispatch(commentsActions.createComment(post)),
        CloseDialog: () => dispatch(commentActions.showDialogCreateComment(false))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCommentDialog);