import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postsActions from './../Redux/Posts';

import Post from './Post';

import CircularProgress from 'material-ui/CircularProgress';

import * as API from './../Api';

class PostList extends Component {

    state = {
        isLoading: true,
        postToEdit: null,
        showEditPostDialog: false
    }

    componentDidMount() {
        API.getPosts().then( res => {
            this.props.FetchPosts(res);
            this.setState({isLoading: false});
        });
    }

    handlePostScore = (postId, option) => {
        API.postVote(postId, option)
            .then(post => this.props.UpdatePost(post));
    };

    handlePostDelete = (postId, option) => {
        API.postDelete(postId)
            .then(post => this.props.UpdatePost(post));
    };

    orderPostsBy = (a , b) => {
        const orderBy = this.props.orderBy;
        return b[orderBy] - a[orderBy];
    }

    render() {
        if (this.state.isLoading) {
            return (<CircularProgress />);
        }

        const params = this.props.params;
        let posts = this.props.postsReducer.posts || [];
    
        if (posts.length > 0) {
            //filtrar por categoria caso seja uma pagina /:category
            if (Object.keys(params).length > 0) {
                posts = posts.filter(post => post.category === params.category);
            }
            posts = posts.filter(post => !post.deleted);

            if (this.props.orderBy != null) {
                posts.sort(this.orderPostsBy);
            }
        }

        if (posts.length === 0) {
            return <h1>No posts to show!</h1>
        }

        return (
            <div>
                {
                    posts.map((post) => 
                        <Post key={post.id} post={post} handlePostDelete={this.handlePostDelete} handlePostScore={this.handlePostScore} isPostList={true} /> 
                    )
                }
            </div>
        );
    };
};

const mapStateToProps = ({postsReducer}) => {
    return {
        postsReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        FetchPosts: (posts) => dispatch(postsActions.fetchPosts(posts)),
        UpdatePost: (post) => dispatch(postsActions.updatePost(post))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostList);