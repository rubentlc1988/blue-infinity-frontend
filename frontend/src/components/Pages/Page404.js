import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as pageTitleActions from './../../Redux/PageTitle';

class Page404 extends Component {
    
    componentDidMount() {
        this.props.ChangeIsErrorPage();
    }

    render() {
        return <h1> Page not exist! </h1>;
    }
};

const mapStateToProps = () => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        ChangeIsErrorPage: (title) => dispatch(pageTitleActions.isErrorPage(true)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Page404);