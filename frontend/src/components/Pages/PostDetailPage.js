import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as pageTitleActions from './../../Redux/PageTitle';
import * as postActions from './../../Redux/Post';
import * as commentActions from './../../Redux/Comment';

import PostComments from './../PostComments';
import AddCommentDialog from './../Dialogs/AddCommentDialog';
import EditPostDialog from './../Dialogs/EditPostDialog';

import Post from './../Post';
import * as API from './../../Api'

import CircularProgress from 'material-ui/CircularProgress';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

import { Redirect } from 'react-router-dom'


class PostPageDetail extends Component {

    state = {
        isLoading: true,
        postNotExist: false,
        postToEdit: null,
        showCreateCommentDialog: false,
        showEditPostDialog: false
    }

    componentDidMount() {
        
        const { id } = this.props.match.params;

        API.getPost(id).then( res => {
            if (res.error || Object.keys(res).length === 0) {
                this.setState({postNotExist: true});
                console.log("PostPageDetail - if");
            } else {
                this.props.ChangePageTitle('Post Detail');
                this.props.FetchPost(res);
                this.setState({isLoading: false});
                console.log("PostPageDetail - else");
            }
        });
        this.props.ChangeIsNotErrorPage();
    }

    handleFloatButtonClick = () => {
        this.props.CreateComment();
    }

    handlePostScore = (postId, option) => {
        API.postVote(postId, option)
            .then(post => this.props.FetchPost(post));
    };

    handlePostDelete = (postId, option) => {
        API.postDelete(postId)
            .then(post => this.props.FetchPost(post));
    };

    render() {
        if (this.state.postNotExist) {
            return <Redirect to='/Page404'/>;
        } else if (this.state.isLoading) {
            return (<CircularProgress />);
        }

        const {post} = this.props.postReducer;
        if (post.deleted) {
            return <h1>Post deleted!</h1>;
        }

        return (
            <div>
                <Post post={post} handlePostDelete={this.handlePostDelete} handlePostScore={this.handlePostScore}/>
                <PostComments postId={post.id} />

                <AddCommentDialog />
                <EditPostDialog />

                <div id="buttonDisplayCover">
                    <FloatingActionButton mini={true} onClick={this.handleFloatButtonClick}>
                        <ContentAdd />
                    </FloatingActionButton>
                </div>
            </div>
        );
    }
};

const mapStateToProps = ({postReducer }) => {
    return {
        postReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        ChangePageTitle: (title) => dispatch(pageTitleActions.changePageTitle(title)),
        ChangeIsNotErrorPage: () => dispatch(pageTitleActions.isErrorPage(false)),
        FetchPost: (post) => dispatch(postActions.fetchPost(post)),
        CreateComment: () => dispatch(commentActions.showDialogCreateComment(true))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostPageDetail);