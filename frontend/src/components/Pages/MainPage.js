import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as pageTitleActions from './../../Redux/PageTitle';
import * as postActions from './../../Redux/Post';

import AddPostDialog from './../Dialogs/AddPostDialog';
import EditPostDialog from './../Dialogs/EditPostDialog';
import PostsList from './../PostsList';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { Redirect } from 'react-router-dom'

class MainPage extends Component {

    state = {
        postNotExist: false,
        showCreatePostDialog: false,
        orderPostsBy: null
    }

    componentDidMount() {
        this.titleDefine();
        this.props.ChangeIsNotErrorPage();
    }

    componentDidUpdate() {
        this.titleDefine();
    }

    titleDefine = () => {
        const { match: { params } } = this.props;
        if (Object.keys(params).length > 0) {
            if (this.props.categoriesReducer.categories.length > 0){
                if (this.props.categoriesReducer.categories.find(cat => cat.name === params.category) === undefined) {
                    this.setState({postNotExist: true});
                }  
            }
            this.props.ChangePageTitle(params.category);    
        } else {
            this.props.ChangePageTitle('All Posts');
        }
    }

    handleFloatButtonClick = () => {
        this.props.CreatePost();
    }

    handleOrderBy = (event, index, value) => {
        this.setState({orderPostsBy: value});
    }

    render() {
        if (this.state.postNotExist) {
            return <Redirect to='/Page404'/>;
        } 

        const { match: { params } } = this.props;

        return (
            <div>
                <div>
                    <h3 id="alignLeft">Order by:</h3>
                    <SelectField
                        id="alignLeft"
                        value={this.state.orderPostsBy}
                        onChange={this.handleOrderBy} >
                            <MenuItem value="timestamp" primaryText="Date" />
                            <MenuItem value="voteScore" primaryText="Score" />
                            <MenuItem value="commentCount" primaryText="Comments" />
                    </SelectField>
                </div>

                <PostsList params={params} orderBy={this.state.orderPostsBy} />

                <AddPostDialog />
                <EditPostDialog />

                <div id="buttonDisplayCover">
                    <FloatingActionButton mini={true} onClick={this.handleFloatButtonClick}>
                        <ContentAdd />
                    </FloatingActionButton>
                </div>
            </div>
        );
    }
};

const mapStateToProps = ({ categoriesReducer }) => {
    return {
        categoriesReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        ChangePageTitle: (title) => dispatch(pageTitleActions.changePageTitle(title)),
        ChangeIsNotErrorPage: () => dispatch(pageTitleActions.isErrorPage(false)),
        CreatePost: () => dispatch(postActions.showDialogCreatePost(true))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);