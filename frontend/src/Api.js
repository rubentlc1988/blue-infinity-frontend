const api = 'http://localhost:3001';

const headers = {
    'Authorization': 'whatever-you-want',
    'Content-Type': 'application/json'
}

/* ---------------------------> CATEGORIES <--------------------------- */
export const getCategories = () => {
    return fetch(`${api}/categories`, { headers })
            .then(res => res.json())
            .then(json => json.categories)
            .catch((e) => {
                console.log("Error:", e)
            })
};

/* ---------------------------> POSTS <--------------------------- */
export const getPosts = () => {
    return fetch(`${api}/posts`, { headers })
            .then(res => res.json())
            .catch((e) => {
                console.log("Error:", e)
            });
};

export const getPost = (postId) => {
    return fetch(`${api}/posts/${postId}`, { headers })
            .then(res => res.json())
            .catch((e) => {
                console.log("Error:", e)
            });
};

export const postVote = (postId, option) => {
    return fetch(`${api}/posts/${postId}`, {
                method: 'POST',
                headers ,
                body: JSON.stringify({option})
            })
            .then((res) => res.json())
            .catch((e) => {
                console.log("Error:", e)
            })
};

export const postDelete = (postId) => {
    return fetch(`${api}/posts/${postId}`, {
                method: 'DELETE',
                headers
            })
            .then((res) => res.json())
            .catch((e) => {
                console.log("Error:", e)
            })
};

export const postUpdate = (postId, post) => {
    return fetch(`${api}/posts/${postId}`, {
                method: 'PUT',
                headers ,
                body: JSON.stringify(post)
            })
            .then((res) => res.json())
            .catch((e) => {
                console.log("Error:", e)
            })
};

export const postCreate = (post) => {
    return fetch(`${api}/posts`, {
                method: 'POST',
                headers ,
                body: JSON.stringify(post)
            })
            .then((res) => res.json())
            .catch((e) => {
                console.log("Error:", e)
            })
};

/* ---------------------------> COMMENTS <--------------------------- */
export const getPostComments = (postId) => {
    return fetch(`${api}/posts/${postId}/comments`, { headers })
            .then(res => res.json())
            .catch((e) => {
                console.log("Error:", e)
            });
};

export const commentVote = (commentId, option) => {
    return fetch(`${api}/comments/${commentId}`, {
                method: 'POST',
                headers ,
                body: JSON.stringify({option})
            })
            .then((res) => res.json())
            .catch((e) => {
                console.log("Error:", e)
            })
};

export const commentDelete = (commentId) => {
    return fetch(`${api}/comments/${commentId}`, {
                method: 'DELETE',
                headers
            })
            .then((res) => res.json())
            .catch((e) => {
                console.log("Error:", e)
            })
};

export const commentUpdate = (commentId, comment) => {
    return fetch(`${api}/comments/${commentId}`, {
                method: 'PUT',
                headers ,
                body: JSON.stringify(comment)
            })
            .then((res) => res.json())
            .catch((e) => {
                console.log("Error:", e)
            })
};

export const commentCreate = (comment) => {
    return fetch(`${api}/comments`, {
                method: 'POST',
                headers ,
                body: JSON.stringify(comment)
            })
            .then((res) => res.json())
            .catch((e) => {
                console.log("Error:", e)
            })
};