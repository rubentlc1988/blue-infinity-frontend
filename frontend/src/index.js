import React from 'react';
import { render } from 'react-dom';

import store from "./Redux/Store";
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom'

import App from './components/App';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import './index.css';

render((
    <Provider store={store}>
        <MuiThemeProvider>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </MuiThemeProvider>
    </Provider>
), document.getElementById('root'));

